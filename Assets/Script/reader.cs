using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class reader : MonoBehaviour
{

    public Text TextHeader;
    string txtcontent;

    void Start()
    {
        TextAsset asset;
        string path=Application.persistentDataPath + "/Log" + System.DateTime.Now.ToString("dd-MM-yyyy");
        #if UNITY_EDITOR
        asset = (TextAsset)Resources.Load("Log" + System.DateTime.Now.ToString("dd-MM-yyyy"));
        txtcontent = asset.text;
        TextHeader.text = txtcontent;
        #endif

        #if UNITY_ANDROID
        asset = Resources.Load(path)as TextAsset;
        txtcontent = asset.text;
        TextHeader.text = txtcontent;
        #endif      

    }
    public void onclickback()
    {
        SceneManager.LoadScene("Init");
    }


}