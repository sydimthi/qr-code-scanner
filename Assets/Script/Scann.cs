﻿using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.IO;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scann : MonoBehaviour
{

    private IScanner BarcodeScanner;
    public Text TextHeader;
    public RawImage Image;
    public AudioSource Audio;
    private float RestartTime, TimeRemains, TimeElapsed;

    private string Name;
    private string Out;
    private string In;
    private string Dy, Mnth, Yer;
    private string[] prday = new string[100];
    private int i = 0, j = 0;
    private bool flag;
    private DateTime date1 = new DateTime(2019, 02, DateTime.Now.Day + 1, 00, 00, 00);
    private DateTime date2 = System.DateTime.Now;


    [SerializeField]
    private string BASE_URL = "https://docs.google.com/forms/d/e/1FAIpQLSezB8I_Ocn2dde-PEa19vbRHwlI4DZ718YygepAH1E7hGD9Cw/formResponse";

    void Awake()
    {
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
    }

    void Start()
    {

        BarcodeScanner = new Scanner();
        BarcodeScanner.Camera.Play();

        BarcodeScanner.OnReady += (sender, arg) =>
        {

            Image.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
            Image.transform.localScale = BarcodeScanner.Camera.GetScale();
            Image.texture = BarcodeScanner.Camera.Texture;

            var rect = Image.GetComponent<RectTransform>();
            var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);

            RestartTime = Time.realtimeSinceStartup;
        };
    }

    private void StartScanner()
    {
        BarcodeScanner.Scan((barCodeType, barCodeValue) =>
        {
            BarcodeScanner.Stop();
            if (TextHeader.text.Length > 250)
            {
                TextHeader.text = "";
            }
            TextHeader.text += barCodeType + ":\t" + barCodeValue + "\t" + System.DateTime.Now + "\n";
            RestartTime += Time.realtimeSinceStartup + 3f;

            Audio.Play();
#if UNITY_ANDROID || UNITY_IOS
			Handheld.Vibrate();
#endif

            Name = barCodeValue;
            Dy = "" + System.DateTime.Now.Day;
            Mnth = "" + System.DateTime.Now.Month;
            Yer = "" + System.DateTime.Now.Year;
            In = System.DateTime.Now.ToString("HH:mm:ss");
            
            string con = Createtext();
            string Content = barCodeValue + System.DateTime.Now + "\n";
            File.AppendAllText(con, Content);
        
            StartCoroutine(processTask(() =>
            {
                SceneManager.LoadScene("Reload");
            }));
            j++;

            for (i = 0; i < 100; i++)
            {
                if (prday[i] == barCodeValue)
                {
                    Debug.Log("exist name");

                    Out = System.DateTime.Now.ToString("HH:mm:ss");
                    Debug.Log(Out);
                    StartCoroutine(Post2(Name, Out));
                    return;
                }
            }
            prday[j] = barCodeValue;
            StartCoroutine(Post1(Name, Dy, Mnth, Yer, In)); 
        });

    }
    IEnumerator Post1(string nme, string dy, string mnth, string yer, string ins)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.2054573001", nme);
        form.AddField("entry.528510286_day", dy);
        form.AddField("entry.528510286_month", mnth);
        form.AddField("entry.528510286_year", yer);
        form.AddField("entry.1876989254", ins);
        byte[] rawData = form.data;
        WWW www = new WWW(BASE_URL, rawData);
        yield return www;
    }
    IEnumerator Post2(string nme, String Ot)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.2054573001", nme);
        form.AddField("entry.1833504966", Ot);
        byte[] rawData = form.data;
        WWW www = new WWW(BASE_URL, rawData);
        yield return www;
        System.Environment.Exit(0);
    }
    string Createtext()
    {
        string path;
       string oldpath;
        check();
        if (flag == true)
        {   
            #if UNITY_EDITOR
            path = Application.dataPath + "/resources/Log" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            #endif
            #if UNITY_ANDROID || UNITY_IOS
            path = Application.persistentDataPath + "/Log" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            #endif
            return (path);
            StartCoroutine(state());
            
        }
        else
        {
            #if UNITY_EDITOR
            oldpath = Application.dataPath + "/resources/Log" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            #endif
            #if UNITY_ANDROID || UNITY_IOS
            oldpath = Application.persistentDataPath + "/log" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            #endif
            return (oldpath);
           
            
        }
    }
    IEnumerator state()
    {
        flag = false;
        String nwDW = System.DateTime.Now.ToString("dd-MM-yyyy");
        WWWForm form = new WWWForm();
        form.AddField("entry.1584612467", nwDW);
        byte[] rawData = form.data;
        WWW iwww = new WWW(BASE_URL, rawData);
        yield return iwww;
        StopCoroutine(state());
        Debug.Log(nwDW);
    }
    private void check()
    {
        int value = DateTime.Compare(date1, date2);
        if (value <= 0)
            flag = true;
        else if (value > 0)
            flag = false;
    }

    void Update()
    {
        if (BarcodeScanner != null)
        {
            BarcodeScanner.Update();
        }
        if (RestartTime != 0 && RestartTime < Time.realtimeSinceStartup)
        {
            RestartTime = 0;
            StartScanner();
        }
    }
    IEnumerator processTask(Action callback)
    {
        Image = null;
        BarcodeScanner.Destroy();
        BarcodeScanner = null;
        yield return new WaitForSeconds(2);
        callback.Invoke();
    }
    public void ClickBack()
    {

        StartCoroutine(StopCamera(() =>
        {
            SceneManager.LoadScene("Init");
        }));
    }


    public IEnumerator StopCamera(Action callback)
    {

        Image = null;
        BarcodeScanner.Destroy();
        BarcodeScanner = null;
        yield return new WaitForSeconds(0.1f);
        callback.Invoke();
    }

}
